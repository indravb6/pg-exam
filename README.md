# PG-Exam

## PMPL Group Project

An e-exam application for making and doing exam through online platform. This application only allowed multi-choice question so the system can automatically grade after the exam has been done.

## Table of Content

-   [Links](#Links)
-   [Requirement](#Requirement)
-   [Framework](#Framework)
-   [Setup](#Setup)
-   [Usage](#Usage)
-   [Developer](#Developer)

## Links

-   [Sonarqube](https://sonarcloud.io/dashboard?id=indravb6_pg-exam)
-   [Deployed App (Heroku)](http://pg-exam.herokuapp.com/)

## Requirement

-   Python v3.8.1 (or latest)

## Framework

-   Django

## Setup

Do the following instructions to set up the application in your local repository

1. Ensure you have already cloned the source code from this repository to your device by using
    ```sh
    git clone https://gitlab.com/indravb6/pg-exam.git
    ```
2. Copy environtment example config to .env file
    ```sh
    cp .env.example .env
    ```
3. Install dependencies
    ```sh
    pip3 install -r requirements.txt
    ```

## Usage

This section will help you to use the application for the development

1. Make migrations to generate model for the database
    ```sh
    python3 manage.py makemigrations
    ```
2. Migrate file for applying database model
    ```sh
    python3 manage.py make
    ```
3. Run tests
    ```sh
    python3 manage.py test
    ```
4. Run application at port 3000
    ```sh
    python3 manage.py runserver
    ```
5. Terminate the application
    ```sh
    [CTRL + C]
    ```

## Developer

Testing With Us

-   Muhammad Indra Ramadhan
-   Rahmat Fadhilah
-   Rizkhi Pramudya Hastiputra
