from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect

from .forms import RegisterForm, LoginForm, EditProfileForm
from .models import User

LOGIN_URI = "/users/login/"

def register(request):
    context = {}
    if request.method == "POST":
        form = RegisterForm(request.POST or None)
        if form.is_valid():
            form.save()
            context["message"] = "Register Successfully"

        context["form"] = form
    else:
        form = RegisterForm()
        context["form"] = form

    return render(request, "register.html", context)

def user_login(request):
    context = {}
    if request.method == "POST":
        form = LoginForm(request.POST or None)
        if form.is_clean():
            username = request.POST["username"]
            password = request.POST["password"]
            user = authenticate(request, username=username, password=password)
            login(request, user)
            context["message"] = "Login Successful"
            return redirect("/")
        else:
            context["message"] = "Invalid Credentials"
        context["form"] = form
    else:
        form = LoginForm()
        context["form"] = form
    
    return render(request, "login.html", context)

@login_required
def edit_profile(request):
    context = {}
    if request.method == "POST":
        form = EditProfileForm(request.POST or None, instance=request.user)
        if form.is_valid():
            form.save()
            context["message"] = "Edited Profile Successfully"

        context["form"] = form
    else:
        form = EditProfileForm()
        context["form"] = form

    return render(request, "edit_profile.html", context)

@login_required
def user_logout(request):
    logout(request)
    return redirect(LOGIN_URI)
