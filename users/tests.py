from django.core.exceptions import ValidationError
from django.test import TestCase, Client

from .apps import UsersConfig
from .forms import RegisterForm, LoginForm, EditProfileForm
from .helper import random_string
from .models import User
from .views import register

user_fields = {"username": "Khiyara",
                "first_name": "Rizkhi",
                "last_name": "P",
                "email": "rizkhi@example.com",
                "password": random_string(10) }

credentials = { "username": user_fields["username"], "password": user_fields["password"] }
edit_profile_data = {"first_name": "first_name", "last_name": "last_name"}

USER_REGISTER_URI = "/users/register/"
USER_LOGIN_URI = "/users/login/"
USER_LOGOUT_URI = "/users/logout/"
USER_EDIT_PROFILE_URI = "/users/edit-profile/"

class UserModelTest(TestCase):
    def test_success_create_user(self):
        User.objects.create(username="Khiyara", first_name="Rizkhi", last_name="H", 
                            email="rizkhi@example.com",
                            password=random_string(10)).full_clean()
        self.assertEqual(User.objects.all().count(), 1)

    def test_fail_create_user(self):
        try:
             User.objects.create(first_name="A").full_clean()
        except ValidationError as e:
            self.assertTrue("email" in e.message_dict)

class UserRegisterFormTest(TestCase):
    def test_registration_form_validation_field_required(self):
        form = RegisterForm(data={"username":""})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors["username"], ["This field is required."])
        
    def test_registration_form_validation_success(self):
        form = RegisterForm(data=user_fields)
        self.assertTrue(form.is_valid())

class UserRegisterViewTest(TestCase):
    def test_can_save_a_POST_request(self):
        self.client.post(USER_REGISTER_URI, data=user_fields)

        self.assertEqual(User.objects.count(), 1)
        new_user = User.objects.first()
        self.assertEqual(new_user.username, "Khiyara")

    def test_can_GET_request(self):
        response = Client().get(USER_REGISTER_URI)
        self.assertTemplateUsed(response, "register.html")

class UserLoginFormTest(TestCase):
    def test_login_form_validation_not_valid(self):
        form = LoginForm(data={"username": "not_exist", "password": random_string(8)})
        self.assertFalse(form.is_clean())

    def test_login_form_validation_not_clean(self):
        form = LoginForm()
        self.assertFalse(form.is_clean())
    
    def test_login_form_validation_valid(self):
        User.objects.create_user(**user_fields)
        form = LoginForm(data={"username": user_fields["username"], "password": user_fields["password"]})
        self.assertTrue(form.is_clean())

class UserLoginViewTest(TestCase):
    def test_user_can_POST_request_login_view(self):
        User.objects.create_user(**user_fields)
        response = self.client.post(USER_LOGIN_URI, data=credentials)
        self.assertEqual(302, response.status_code)
        self.assertEqual("/", response.url)

    def test_user_cannot_POST_request_login_view(self):
        response = self.client.post(USER_LOGIN_URI, data=credentials)
        self.assertFalse("Rizkhi" in response.content.decode())
        self.assertTrue("Invalid Credentials" in response.content.decode())

    def test_user_can_GET_request_login_view(self):
        response = Client().get(USER_LOGIN_URI)
        self.assertTemplateUsed(response, "login.html")

class UserLogoutViewTest(TestCase):
    def test_user_can_logout(self):
        User.objects.create_user(**user_fields)
        self.client.login(**credentials)
        response = self.client.get(USER_LOGOUT_URI)
        self.assertEqual(USER_LOGIN_URI, response.url)
        self.assertEqual(302, response.status_code)

    def test_user_must_login_so_can_logout(self):
        response = self.client.get(USER_LOGOUT_URI)
        self.assertNotEqual(USER_LOGIN_URI, response.url)
        self.assertEqual(302, response.status_code)

class UserEditProfileFormTest(TestCase):
    def test_edit_profile_validation_not_valid_with_empty_first_name(self):
        form = EditProfileForm(data={"first_name": "", "last_name": "last_name"})
        self.assertFalse(form.is_valid())

    def test_edit_profile_validation_not_valid_with_empty_last_name(self):
        form = EditProfileForm(data={"first_name": "first_name", "last_name": ""})
        self.assertFalse(form.is_valid())

    def test_edit_profile_validation_valid(self):
        User.objects.create_user(**user_fields)
        self.client.login(**credentials)
        form = EditProfileForm(data={"first_name": "first_name", "last_name": "last_name"})
        self.assertTrue(form.is_valid())

class UserEditProfileViewTest(TestCase):
    def test_user_can_POST_request_edit_profile_view(self):
        User.objects.create_user(**user_fields)
        self.client.login(**credentials)
        response = self.client.post(USER_EDIT_PROFILE_URI, data=edit_profile_data)
        self.assertTrue("first_name" in response.content.decode())
        self.assertTrue("Edited Profile Successfully" in response.content.decode())

    def test_user_cannot_POST_request_before_login(self):
        response = self.client.post(USER_EDIT_PROFILE_URI, data=edit_profile_data)
        self.assertFalse("first_name" in response.content.decode())
        self.assertEqual(302, response.status_code)
        self.assertEqual("{}?next={}".format(USER_LOGIN_URI, USER_EDIT_PROFILE_URI), response.url)

    def test_user_can_GET_request_edit_profile_view(self):
        User.objects.create_user(**user_fields)
        self.client.login(**credentials)
        response = self.client.get(USER_EDIT_PROFILE_URI)
        self.assertTemplateUsed(response, "edit_profile.html")

    def test_user_cannot_GET_request_edit_profile_before_login(self):
        response = Client().get(USER_EDIT_PROFILE_URI)
        self.assertEqual(302, response.status_code)
        self.assertEqual("{}?next={}".format(USER_LOGIN_URI, USER_EDIT_PROFILE_URI), response.url)

class UserAppsTest(TestCase):
    def test_used_app_name(self):
        name = UsersConfig.name
        self.assertEqual(name, 'users')
