import secrets
import string

def random_string(size=10, chars=string.printable):
    return ''.join(secrets.choice(chars) for _ in range(size))
