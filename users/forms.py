from django import forms
from django.contrib.auth import authenticate, get_user_model
from django.contrib.auth.forms import AuthenticationForm
from django.utils.datastructures import MultiValueDictKeyError

from .models import User

class RegisterForm(forms.ModelForm):
    username_attrs = {"type": "text", "class": "form-control", "placeholder": "Username"}
    username = forms.CharField(widget=forms.TextInput(attrs=username_attrs), max_length=20, required=True)

    first_name_attrs = {"type": "text", "class": "form-control", "placeholder": "First Name"}
    first_name = forms.CharField(widget=forms.TextInput(attrs=first_name_attrs), max_length=32, required=True)

    last_name_attrs = {"type": "text", "class": "form-control", "placeholder": "Last Name"}
    last_name = forms.CharField(widget=forms.TextInput(attrs=last_name_attrs), max_length=32, required=True)

    email_attrs = {"type": "email", "class": "form-control", "placeholder": "Email"}
    email = forms.EmailField(widget=forms.EmailInput(attrs=email_attrs), max_length=64, required=True)

    password_attrs = {"type": "password", "class": "form-control", "placeholder": "Password"}
    password = forms.CharField(widget=forms.PasswordInput(attrs=password_attrs), min_length=8, max_length=16, required=True)

    class Meta:
        model = User
        fields = ("username", "first_name", "last_name", "email", "password",)

    def save(self, commit=True):
        user = super(RegisterForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()

        return user

class LoginForm(AuthenticationForm):
    username_attrs = {"type": "text", "class": "form-control form-control-sm", "placeholder": "Username"}
    username = forms.EmailField(widget=forms.TextInput(attrs=username_attrs), max_length=64, required=True)

    password_attrs = {"type": "password", "class": "form-control form-control-sm", "placeholder": "Password"}
    password = forms.CharField(widget=forms.PasswordInput(attrs=password_attrs), min_length=8, max_length=16, required=True)

    def __init__(self, request=None, *args, **kwargs):
        self.request = request
        self.user_cache = None
        super(AuthenticationForm, self).__init__(*args, **kwargs)

        user_model = get_user_model()
        self.username_field = user_model._meta.get_field(user_model.USERNAME_FIELD)

    def is_clean(self):
        username = None
        password = None
        try:
            username = self.data["username"]
            password = self.data["password"]
        except MultiValueDictKeyError:
            username = self.request and self.request["username"]
            password = self.request and self.request["password"]

        if username and password:
            self.user_cache = authenticate(username=username,
                                           password=password)

            if self.user_cache:
                return True

        return False

class EditProfileForm(forms.ModelForm):
    first_name_attrs = {"type": "text", "class": "form-control", "placeholder": "First Name"}
    first_name = forms.CharField(widget=forms.TextInput(attrs=first_name_attrs), max_length=32, required=True)

    last_name_attrs = {"type": "text", "class": "form-control", "placeholder": "Last Name"}
    last_name = forms.CharField(widget=forms.TextInput(attrs=last_name_attrs), max_length=32, required=True)

    class Meta:
        model = User
        fields = ("first_name", "last_name",)