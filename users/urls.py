from django.urls import path, re_path
from django.views.generic.base import RedirectView

from . import views

urlpatterns = [
    path("register/", views.register, name="register"),
    path("login/", views.user_login, name="login"),
    path("logout/", views.user_logout, name="logout"),
    path("edit-profile/", views.edit_profile, name="edit_profile"),
]
