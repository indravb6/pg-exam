from django.shortcuts import render, HttpResponseRedirect
from django.urls import reverse

def home(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse('login'))
    
    return render(request, "home.html")