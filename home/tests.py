from django.core.exceptions import ValidationError
from django.test import TestCase, Client

from users.models import User
from .apps import HomeConfig
from pgexam.dummy import user_fields, credentials

USER_LOGIN_URI = "/users/login/"

class HomeViewTest(TestCase):
    def test_guest_redirect_to_login(self):
        response = self.client.get("/")
        self.assertEqual(USER_LOGIN_URI, response.url)
        self.assertEqual(302, response.status_code)
    
    def test_user_can_access_home(self):
        User.objects.create_user(**user_fields)
        self.client.login(**credentials)
        response = self.client.get("/")
        self.assertTemplateUsed(response, "home.html")
        self.assertIn(user_fields["first_name"], response.content.decode())

class HomeAppsTest(TestCase):
    def test_used_app_name(self):
        name = HomeConfig.name
        self.assertEqual(name, 'home')
