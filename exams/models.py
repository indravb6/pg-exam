from django.db import models
from users.models import User


class Exam(models.Model):
    title = models.CharField(max_length=30)
    code = models.CharField(max_length=20, unique=True)
    data = models.TextField()
    start = models.DateTimeField()
    duration = models.PositiveIntegerField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)


class ExamResponse(models.Model):
    code = models.CharField(max_length=20)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    data = models.TextField()
    score = models.FloatField()
