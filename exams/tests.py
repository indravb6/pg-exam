from django.core.exceptions import ValidationError
from django.test import TestCase, Client
from django.utils import timezone
from datetime import datetime, timedelta
import json

from users.helper import random_string
from users.models import User
from .apps import ExamsConfig
from .models import Exam
from pgexam.dummy import user_fields, credentials

USER_LOGIN_URI = "/users/login/"
EXAM_SEARCH_URI = "/exams/search?code="
EXAM_ACCESS_URI = "/exams/access?code="
EXAM_LIST_URI = "/exams/"
EXAM_MY_LIST_URI = EXAM_LIST_URI + "?type=my-exam"
EXAM_TAKEN_LIST_URI = EXAM_LIST_URI + "?type=taken-exam"
EXAM_RESULT_URI = "/exams/result/"


class CreateExamViewTest(TestCase):
    def test_guest_can_not_create_exam(self):
        response = self.client.get("/exams/create?count=5")
        self.assertIn(USER_LOGIN_URI, response.url)
        self.assertEqual(302, response.status_code)

    def test_user_can_get_exam_page(self):
        User.objects.create_user(**user_fields)
        self.client.login(**credentials)
        response = self.client.get("/exams/create?count=3")
        self.assertTemplateUsed(response, "create_exam.html")
        self.assertIn("Soal nomor 1", response.content.decode())
        self.assertIn("Soal nomor 2", response.content.decode())
        self.assertIn("Soal nomor 3", response.content.decode())

    def test_user_can_create_exam(self):
        User.objects.create_user(**user_fields)
        self.client.login(**credentials)
        data = {
            "title": "exam sample 1",
            "code": "SAMPLE1",
            "start": "2020-01-01 02:00:00Z",
            "duration": 60,
            "data": [{
                "question": "question no 1",
                "option": ["op a", "op b", "op c", "op d"],
                "answer": "A",
            }],
        }
        response = self.client.post("/exams/create",
                                    json.dumps(data),
                                    content_type="application/json")
        self.assertEqual(302, response.status_code)
        self.assertEqual("/", response.url)

    def test_user_can_not_create_exam_without_title(self):
        User.objects.create_user(**user_fields)
        self.client.login(**credentials)
        data = {
            "title": "",
            "code": "SAMPLE1",
            "start": "2020-01-01 02:00:00Z",
            "duration": 60,
            "data": [{
                "question": "question no 1",
                "option": ["op a", "op b", "op c", "op d"],
                "answer": "A",
            }],
        }
        response = self.client.post("/exams/create",
                                    json.dumps(data),
                                    content_type="application/json")
        self.assertEqual(400, response.status_code)

    def test_user_can_not_create_exam_with_negative_duration(self):
        User.objects.create_user(**user_fields)
        self.client.login(**credentials)
        data = {
            "title": "",
            "code": "SAMPLE1",
            "start": "2020-01-01 02:00:00Z",
            "duration": -60,
            "data": [{
                "question": "question no 1",
                "option": ["op a", "op b", "op c", "op d"],
                "answer": "A",
            }],
        }
        response = self.client.post("/exams/create",
                                    json.dumps(data),
                                    content_type="application/json")
        self.assertEqual(400, response.status_code)

    def test_user_can_not_create_exam_with_invalid_answer(self):
        User.objects.create_user(**user_fields)
        self.client.login(**credentials)
        data = {
            "title": "exam sample 1",
            "code": "SAMPLE1",
            "start": "2020-01-01 02:00:00Z",
            "duration": 60,
            "data": [{
                "question": "question no 1",
                "option": ["op a", "op b", "op c", "op d"],
                "answer": "E",
            }],
        }
        response = self.client.post("/exams/create",
                                    json.dumps(data),
                                    content_type="application/json")
        self.assertEqual(400, response.status_code)

    def test_user_can_not_create_exam_with_option_less_than_4(self):
        User.objects.create_user(**user_fields)
        self.client.login(**credentials)
        data = {
            "title": "exam sample 1",
            "code": "SAMPLE1",
            "start": "2020-01-01 02:00:00Z",
            "duration": 60,
            "data": [{
                "question": "question no 1",
                "option": ["op a", "op b", "op c"],
                "answer": "A",
            }],
        }
        response = self.client.post("/exams/create",
                                    json.dumps(data),
                                    content_type="application/json")
        self.assertEqual(400, response.status_code)

    def test_user_can_not_create_exam_with_option_more_than_5(self):
        User.objects.create_user(**user_fields)
        self.client.login(**credentials)
        data = {
            "title": "exam sample 1",
            "code": "SAMPLE1",
            "start": "2020-01-01 02:00:00Z",
            "duration": 60,
            "data": [{
                "question": "question no 1",
                "option": ["op a", "op b", "op c", "op d", "op e"],
                "answer": "A",
            }],
        }
        response = self.client.post("/exams/create",
                                    json.dumps(data),
                                    content_type="application/json")
        self.assertEqual(400, response.status_code)

    def test_user_can_not_create_exam_with_same_code(self):
        User.objects.create_user(**user_fields)
        self.client.login(**credentials)
        data = {
            "title": "exam sample 1",
            "code": "SAMPLE1",
            "start": "2020-01-01 02:00:00Z",
            "duration": 60,
            "data": [{
                "question": "question no 1",
                "option": ["op a", "op b", "op c", "op d"],
                "answer": "A",
            }],
        }
        response = self.client.post("/exams/create",
                                    json.dumps(data),
                                    content_type="application/json")
        self.assertEqual(302, response.status_code)

        response = self.client.post("/exams/create",
                                    json.dumps(data),
                                    content_type="application/json")
        self.assertEqual(400, response.status_code)
        self.assertEqual("CODE is already taken", response.content.decode())


class SearchExamViewTest(TestCase):
    def test_user_can_search_exam(self):
        User.objects.create_user(**user_fields)
        self.client.login(**credentials)
        user = User.objects.first()
        data = json.dumps([{
            "question": "question no 1",
            "option": ["op a", "op b", "op c", "op d"],
            "answer": "A",
        }])

        Exam.objects.create(
            title="SAMPLE1",
            code="Exam1",
            data=data,
            start=datetime.now(tz=timezone.utc),
            duration=50,
            user=user,
        )

        response = self.client.get(EXAM_SEARCH_URI + "Exam1")

        self.assertEqual(200, response.status_code)
        self.assertTrue("SAMPLE1" in response.content.decode())

    def test_user_can_search_exam_but_not_found(self):
        User.objects.create_user(**user_fields)
        self.client.login(**credentials)

        response = self.client.get(EXAM_SEARCH_URI + "Exam1")

        self.assertEqual(404, response.status_code)

    def test_user_can_get_search_page(self):
        User.objects.create_user(**user_fields)
        self.client.login(**credentials)

        response = self.client.get("/exams/search")

        self.assertEqual(200, response.status_code)
        self.assertTemplateUsed(response, "search_exam.html")


class ListExamViewTest(TestCase):
    def test_user_can_see_list_of_exam(self):
        User.objects.create_user(**user_fields)
        self.client.login(**credentials)
        user = User.objects.first()
        data = json.dumps([{
            "question": "question no 1",
            "option": ["op a", "op b", "op c", "op d"],
            "answer": "A",
        }])

        Exam.objects.create(
            title="SAMPLE1",
            code="Exam1",
            data=data,
            start=datetime.now(tz=timezone.utc),
            duration=50,
            user=user,
        )
        Exam.objects.create(
            title="SAMPLE2",
            code="Exam2",
            data=data,
            start=datetime.now(tz=timezone.utc),
            duration=60,
            user=user,
        )

        response = self.client.get(EXAM_MY_LIST_URI)

        self.assertEqual(200, response.status_code)
        self.assertTrue("SAMPLE1" in response.content.decode())
        self.assertTrue("SAMPLE2" in response.content.decode())

    def test_user_cannot_see_other_users_exam(self):
        another_user_fields = {
            "username": "Budi",
            "first_name": "Budi",
            "last_name": "P.",
            "email": "budi@example.com",
            "password": random_string(10),
        }
        data = json.dumps([{
            "question": "question no 1",
            "option": ["op a", "op b", "op c", "op d"],
            "answer": "A",
        }])
        another_credentials = {
            "username": another_user_fields["username"],
            "password": another_user_fields["password"],
        }
        User.objects.create_user(**user_fields)
        user = User.objects.first()
        User.objects.create_user(**another_user_fields)
        self.client.login(**another_credentials)

        Exam.objects.create(
            title="SAMPLE1",
            code="Exam1",
            data=data,
            start=datetime.now(tz=timezone.utc),
            duration=50,
            user=user,
        )

        response = self.client.get(EXAM_MY_LIST_URI)

        self.assertEqual(200, response.status_code)
        self.assertFalse("SAMPLE1" in response.content.decode())

    def test_user_see_can_see_list_of_taken_exam(self):
        another_user_fields = {
            "username": "Budi",
            "first_name": "Budi",
            "last_name": "P.",
            "email": "budi@example.com",
            "password": random_string(10),
        }
        data = json.dumps([{
            "question": "question no 1",
            "option": ["op a", "op b", "op c", "op d"],
            "answer": "A",
        }])
        another_credentials = {
            "username": another_user_fields["username"],
            "password": another_user_fields["password"],
        }

        exam_response_data = json.dumps({"code": "Exam1", "answers": ["A"]})

        User.objects.create_user(**user_fields)
        user = User.objects.first()
        User.objects.create_user(**another_user_fields)
        self.client.login(**another_credentials)
        Exam.objects.create(
            title="SAMPLE1",
            code="Exam1",
            data=data,
            start=datetime.now(tz=timezone.utc),
            duration=50,
            user=user,
        )

        response = self.client.post(
            EXAM_ACCESS_URI + "Exam1",
            exam_response_data,
            content_type="application/json",
        )

        response = self.client.get(EXAM_TAKEN_LIST_URI)

        self.assertEqual(200, response.status_code)
        self.assertTrue("SAMPLE1" in response.content.decode())
        self.assertTrue("100" in response.content.decode())

    def test_user_can_get_list_page(self):
        User.objects.create_user(**user_fields)
        self.client.login(**credentials)

        response = self.client.get("/exams/")

        self.assertEqual(200, response.status_code)
        self.assertTemplateUsed(response, "list_exam.html")

    def test_user_cannot_user_another_type_list_exam(self):
        User.objects.create_user(**user_fields)
        self.client.login(**credentials)

        response = self.client.get(EXAM_LIST_URI + "?type=not_exist_type")

        self.assertEqual(400, response.status_code)


class ExamAppsTest(TestCase):
    def test_used_app_name(self):
        name = ExamsConfig.name
        self.assertEqual(name, "exams")


class AccessExamTest(TestCase):
    def test_user_can_not_access_exam_before_start_date(self):
        User.objects.create_user(**user_fields)
        self.client.login(**credentials)
        user = User.objects.first()
        data = json.dumps([{
            "question": "question no 1",
            "option": ["op a", "op b", "op c", "op d"],
            "answer": "A",
        }])

        Exam.objects.create(
            title="SAMPLE1",
            code="Exam1",
            data=data,
            start=datetime.now(tz=timezone.utc) + timedelta(hours=1),
            duration=50,
            user=user,
        )

        response = self.client.get(EXAM_ACCESS_URI + "Exam1")

        self.assertEqual(400, response.status_code)

    def test_user_can_not_access_exam_after_end_date(self):
        User.objects.create_user(**user_fields)
        self.client.login(**credentials)
        user = User.objects.first()
        data = json.dumps([{
            "question": "question no 1",
            "option": ["op a", "op b", "op c", "op d"],
            "answer": "A",
        }])

        Exam.objects.create(
            title="SAMPLE1",
            code="Exam1",
            data=data,
            start=datetime.now(tz=timezone.utc) - timedelta(hours=1),
            duration=50,
            user=user,
        )

        response = self.client.get(EXAM_ACCESS_URI + "Exam1")

        self.assertEqual(400, response.status_code)

    def test_user_can_access_exam(self):
        User.objects.create_user(**user_fields)
        self.client.login(**credentials)
        user = User.objects.first()
        data = json.dumps([{
            "question": "question no 1",
            "option": ["op a", "op b", "op c", "op d"],
            "answer": "A",
        }])

        Exam.objects.create(
            title="SAMPLE1",
            code="Exam1",
            data=data,
            start=datetime.now(tz=timezone.utc),
            duration=50,
            user=user,
        )

        response = self.client.get(EXAM_ACCESS_URI + "Exam1")

        self.assertEqual(200, response.status_code)
        self.assertTemplateUsed(response, "access_exam.html")

    def test_user_can_submit_exam(self):
        User.objects.create_user(**user_fields)
        self.client.login(**credentials)
        user = User.objects.first()
        data = json.dumps([{
            "question": "question no 1",
            "option": ["op a", "op b", "op c", "op d"],
            "answer": "A",
        }])

        Exam.objects.create(
            title="SAMPLE1",
            code="Exam1",
            data=data,
            start=datetime.now(tz=timezone.utc),
            duration=50,
            user=user,
        )

        examResponseData = json.dumps({"code": "Exam1", "answers": ["A"]})

        response = self.client.post(EXAM_ACCESS_URI + "Exam1",
                                    examResponseData,
                                    content_type="application/json")

        self.assertEqual(302, response.status_code)
        self.assertEqual("/", response.url)

    def test_user_can_not_submit_exam_before_start_date(self):
        User.objects.create_user(**user_fields)
        self.client.login(**credentials)
        user = User.objects.first()
        data = json.dumps([{
            "question": "question no 1",
            "option": ["op a", "op b", "op c", "op d"],
            "answer": "A",
        }])

        Exam.objects.create(
            title="SAMPLE1",
            code="Exam1",
            data=data,
            start=datetime.now(tz=timezone.utc) + timedelta(hours=1),
            duration=50,
            user=user,
        )

        examResponseData = json.dumps({"code": "Exam1", "answers": ["A"]})

        response = self.client.post(EXAM_ACCESS_URI + "Exam1",
                                    examResponseData,
                                    content_type="application/json")

        self.assertEqual(400, response.status_code)

    def test_user_can_not_submit_exam_after_end_date(self):
        User.objects.create_user(**user_fields)
        self.client.login(**credentials)
        user = User.objects.first()
        data = json.dumps([{
            "question": "question no 1",
            "option": ["op a", "op b", "op c", "op d"],
            "answer": "A",
        }])

        Exam.objects.create(
            title="SAMPLE1",
            code="Exam1",
            data=data,
            start=datetime.now(tz=timezone.utc) - timedelta(hours=1),
            duration=50,
            user=user,
        )

        examResponseData = json.dumps({"code": "Exam1", "answers": ["A"]})

        response = self.client.post(EXAM_ACCESS_URI + "Exam1",
                                    examResponseData,
                                    content_type="application/json")

        self.assertEqual(400, response.status_code)

    def test_user_cannot_submit_exam(self):
        User.objects.create_user(**user_fields)
        self.client.login(**credentials)

        examResponseData = json.dumps({"code": "Exam1", "answers": ["A"]})

        response = self.client.post(EXAM_ACCESS_URI + "Exam1",
                                    examResponseData,
                                    content_type="application/json")

        self.assertEqual(404, response.status_code)

    def test_get_exam_not_found(self):
        User.objects.create_user(**user_fields)
        self.client.login(**credentials)

        response = self.client.get(EXAM_ACCESS_URI + "Exam1")

        self.assertEqual(404, response.status_code)

    def test_get_exam_without_query(self):
        User.objects.create_user(**user_fields)
        self.client.login(**credentials)

        response = self.client.get("/exams/access")

        self.assertEqual(200, response.status_code)
        self.assertTemplateUsed(response, "access_exam.html")
        self.assertIn("Exam not found", response.content.decode())

    def test_user_cannot_submit_exam_twice(self):
        User.objects.create_user(**user_fields)
        self.client.login(**credentials)
        user = User.objects.first()
        data = json.dumps([{
            "question": "question no 1",
            "option": ["op a", "op b", "op c", "op d"],
            "answer": "A",
        }])

        Exam.objects.create(
            title="SAMPLE1",
            code="Exam1",
            data=data,
            start=datetime.now(tz=timezone.utc),
            duration=50,
            user=user,
        )

        examResponseData = json.dumps({"code": "Exam1", "answers": ["A"]})

        self.client.post(EXAM_ACCESS_URI + "Exam1",
                         examResponseData,
                         content_type="application/json")

        response = self.client.post(EXAM_ACCESS_URI + "Exam1",
                                    examResponseData,
                                    content_type="application/json")

        self.assertEqual(400, response.status_code)
        self.assertTrue("EXAM already taken" in response.content.decode())


class ResultExamViewTest(TestCase):
    def test_user_can_see_result_of_exams(self):
        User.objects.create_user(**user_fields)
        self.client.login(**credentials)
        user = User.objects.first()
        data = json.dumps([{
            "question": "question no 1",
            "option": ["op a", "op b", "op c", "op d"],
            "answer": "A",
        }])

        Exam.objects.create(
            title="SAMPLE1",
            code="Exam1",
            data=data,
            start=datetime.now(tz=timezone.utc),
            duration=50,
            user=user,
        )

        exam_response_data = json.dumps({"code": "Exam1", "answers": ["A"]})

        self.client.post(
            EXAM_ACCESS_URI + "Exam1",
            exam_response_data,
            content_type="application/json",
        )
        exam = Exam.objects.first()

        response = self.client.get(EXAM_RESULT_URI + str(exam.id))

        self.assertEqual(200, response.status_code)
        self.assertTrue("Andi" in response.content.decode())
        self.assertTrue("100.0" in response.content.decode())

    def test_user_cannot_see_other_users_result_exam(self):
        User.objects.create_user(**user_fields)
        another_user_fields = {
            "username": "Caca",
            "first_name": "Caca",
            "last_name": "S.",
            "email": "caca@example.com",
            "password": random_string(10),
        }
        another_credentials = {
            "username": another_user_fields["username"],
            "password": another_user_fields["password"],
        }
        user = User.objects.first()
        User.objects.create_user(**another_user_fields)
        self.client.login(**another_credentials)

        data = json.dumps([{
            "question": "question no 1",
            "option": ["op a", "op b", "op c", "op d"],
            "answer": "A",
        }])

        Exam.objects.create(
            title="SAMPLE1",
            code="Exam1",
            data=data,
            start=datetime.now(tz=timezone.utc),
            duration=50,
            user=user,
        )
        exam = Exam.objects.first()

        response = self.client.get(EXAM_RESULT_URI + str(exam.id))

        self.assertEqual(403, response.status_code)

    def test_user_can_see_result_of_exams_with_many_participant(self):
        User.objects.create_user(**user_fields)
        self.client.login(**credentials)
        user = User.objects.first()
        data = json.dumps([{
            "question": "question no 1",
            "option": ["op a", "op b", "op c", "op d"],
            "answer": "A",
        }])
        another_user_fields = {
            "username": "Caca",
            "first_name": "Caca",
            "last_name": "P.",
            "email": "caca@example.com",
            "password": random_string(10),
        }
        another_credentials = {
            "username": another_user_fields["username"],
            "password": another_user_fields["password"],
        }
        User.objects.create_user(**another_user_fields)

        Exam.objects.create(
            title="SAMPLE1",
            code="Exam1",
            data=data,
            start=datetime.now(tz=timezone.utc),
            duration=50,
            user=user,
        )

        exam_response_data = json.dumps({"code": "Exam1", "answers": ["A"]})

        self.client.post(
            EXAM_ACCESS_URI + "Exam1",
            exam_response_data,
            content_type="application/json",
        )
        self.client.logout()
        self.client.login(**another_credentials)
        exam_response_data = json.dumps({"code": "Exam1", "answers": ["B"]})
        self.client.post(
            EXAM_ACCESS_URI + "Exam1",
            exam_response_data,
            content_type="application/json",
        )

        exam = Exam.objects.first()

        self.client.logout()
        self.client.login(**credentials)

        response = self.client.get(EXAM_RESULT_URI + str(exam.id))

        self.assertEqual(200, response.status_code)
        self.assertTrue("Andi" in response.content.decode())
        self.assertTrue("Caca" in response.content.decode())
        self.assertTrue("100.0" in response.content.decode())
        self.assertTrue("0.0" in response.content.decode())
