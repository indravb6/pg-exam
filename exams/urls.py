from django.urls import path

from . import views

urlpatterns = [
    path("", views.list_exam, name="list-exam"),
    path("result/<int:exam_id>", views.result_exam, name="result-exam"),
    path("create", views.create_exam, name="create-exam"),
    path("search", views.search_exam, name="search-exam"),
    path("access", views.access_exam, name="access-exam"),
]
