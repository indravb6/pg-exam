from django import forms
from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.core.paginator import Paginator
from django.shortcuts import render, redirect
from django.http import HttpResponseBadRequest, JsonResponse, Http404
from django.views.decorators.http import require_GET
from itertools import chain
from jsonschema import validate
from jsonschema.exceptions import ValidationError
from datetime import datetime, timedelta
from django.utils import timezone
import json

from .validators import exam_schema
from .models import Exam, ExamResponse
from users.models import User


@login_required
def create_exam(request):
    if request.method == "GET":
        count = int(request.GET["count"])
        context = {"range": range(1, 1 + count), "count": count}
        return render(request, "create_exam.html", context)

    body = json.loads(request.body)

    try:
        validate(body, exam_schema)
    except ValidationError:
        return HttpResponseBadRequest()

    if Exam.objects.filter(code=body["code"]).count() > 0:
        return HttpResponseBadRequest("CODE is already taken")

    exam = Exam(
        title=body["title"],
        code=body["code"],
        start=body["start"],
        duration=body["duration"],
        data=json.dumps(body["data"]),
        user_id=request.user.id,
    )
    exam.save()

    return redirect("/")


@login_required
@require_GET
def search_exam(request):
    if "code" not in request.GET:
        return render(request, "search_exam.html")

    code = request.GET["code"] or ""
    exam = Exam.objects.filter(code=code)

    if exam.count() == 0:
        raise Http404()

    exam = exam.get()
    data = {
        "title": exam.title,
        "code": exam.code,
        "start": exam.start,
        "duration": exam.duration,
    }

    return JsonResponse(data)


def hide_answers(data):
    newdata = []
    for item in data:
        newdata.append({
            "question": item["question"],
            "option": item["option"]
        })

    return newdata


def find_score(answers, keys):
    total = len(keys)
    score = 0
    for i in range(total):
        if answers[i].upper() == keys[i]["answer"].upper():
            score += 1

    return score * 100 / total


@login_required
def access_exam(request):
    context = {}
    if request.method == "POST":
        body = json.loads(request.body)
        code = body["code"]

        exam = Exam.objects.filter(code=code)

        if exam.count() == 0:
            raise Http404()

        exam = exam.get()

        end_time = exam.start + timedelta(minutes=exam.duration)

        if not (exam.start <= timezone.now() <= end_time):
            return HttpResponseBadRequest("Can't submit exam")

        questions = json.loads(exam.data)

        answers = body["answers"]
        score = find_score(answers, questions)

        taken_exam = ExamResponse.objects.filter(user=request.user.id,
                                                 code=code)
        if taken_exam.count() >= 1:
            return HttpResponseBadRequest("EXAM already taken")

        exam_response = ExamResponse(
            code=code,
            data=json.dumps(answers),
            score=score,
            user_id=request.user.id,
        )

        exam_response.save()

        return redirect("/")

    if "code" not in request.GET:
        context["error"] = "Exam not found"
        return render(request, "access_exam.html", context)

    code = request.GET["code"] or ""
    exam = Exam.objects.filter(code=code)

    if exam.count() == 0:
        raise Http404()

    exam = exam.get()
    end_time = exam.start + timedelta(minutes=exam.duration)

    if not (exam.start <= timezone.now() <= end_time):
        return HttpResponseBadRequest("Can't access exam")

    questions = json.loads(exam.data)
    question_data = hide_answers(questions)

    context["data"] = {
        "title": exam.title,
        "code": exam.code,
        "start": exam.start,
        "duration": exam.duration,
        "data": question_data,
        "total": len(questions),
    }

    return render(request, "access_exam.html", context)


@login_required
@require_GET
def list_exam(request):
    context = {}
    page_type = request.GET.get("type", "my-exam")
    page_number = forms.IntegerField(min_value=1).clean(
        request.GET.get("page", 1))
    exams = None
    if page_type == "my-exam":
        exams = Exam.objects.filter(user=request.user.id).order_by("id")
    elif page_type == "taken-exam":
        exam_responses = ExamResponse.objects.filter(user=request.user.id)
        taken_exam = Exam.objects.filter(
            code__in=[val["code"]
                      for val in exam_responses.values()]).order_by("id")
        for taken in taken_exam:
            for exam in exam_responses:
                if taken.code == exam.code:
                    taken.score = exam.score
                    break
        exams = taken_exam

    if exams == None:
        return HttpResponseBadRequest("Type didn't exists")

    paginator = Paginator(exams, 10)
    context["page_obj"] = paginator.get_page(page_number)
    context["type"] = page_type

    return render(request, "list_exam.html", context)


@login_required
@require_GET
def result_exam(request, exam_id):
    context = {}
    page_number = forms.IntegerField(min_value=1).clean(
        request.GET.get("page", 1))

    exam = Exam.objects.filter(id=exam_id).get()

    if exam.user.id != request.user.id:
        raise PermissionDenied()

    exam_responses = ExamResponse.objects.filter(code=exam.code)
    users = User.objects.filter(
        id__in=[val["user_id"]
                for val in exam_responses.values()]).order_by('username')

    for user in users:
        for exam in exam_responses:
            if user.id == exam.user_id:
                user.score = exam.score
                break

    paginator = Paginator(users, 10)
    context["page_obj"] = paginator.get_page(page_number)

    return render(request, "result_exam.html", context)
