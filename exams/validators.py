exam_schema = {
    "type": "object",
    "additionalProperties": False,
    "required": ["title", "code", "data"],
    "properties": {
        "title": { "type": "string", "minLength": 1, "maxLength": 30 },
        "code": { "type": "string", "minLength": 1, "maxLength": 20 },
        "start": { "type": "string", "format": "date" },
        "duration": { "type": "integer", "min": 1 },
        "data": {
            "type": "array",
            "items": {
                "type": "object",
                "additionalProperties": False,
                "required": ["question", "answer", "option"],
                "properties": {
                    "question": { "type": "string", "minLength": 1 },
                    "answer": { "type": "string", "enum": ["A", "B", "C", "D"] },
                    "option": {
                        "type": "array",
                        "minItems": 4,
                        "maxItems": 4,
                        "items": { "type": "string", "minLength": 1 }
                    }
                }   
            }
        }
    }
}
